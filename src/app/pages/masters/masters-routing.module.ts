import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MastersComponent } from './masters.component';
import { TongQuanComponent } from './tong-quan/tong-quan.component';
import { HanghoaListComponent } from './hang-hoa/hanghoa-list/hanghoa-list.component';



const routes: Routes = [
  {path: '', component: MastersComponent, children: [
    {path: '', redirectTo: 'tongquan', pathMatch: 'full'},
    {path: 'tongquan', component: TongQuanComponent},
    // {path:'hanghoa', component:HanghoaListComponent}
    {path: 'hanghoa', loadChildren: './hang-hoa/hanghoa.module#HanghoaModule'},
  ]},
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule, 
    RouterModule.forChild(routes)
  ],
exports: [RouterModule],
})
export class MastersRoutingModule { }
