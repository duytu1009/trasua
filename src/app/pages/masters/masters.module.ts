import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MastersRoutingModule } from './masters-routing.module';
import { MastersComponent } from './masters.component';
import { NavbarComponent } from './element/navbar/navbar.component';
import { TongQuanComponent } from './tong-quan/tong-quan.component';
import { HanghoaListComponent } from './hang-hoa/hanghoa-list/hanghoa-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    MastersComponent,
    NavbarComponent,
    TongQuanComponent,
  ],
  imports: [
    CommonModule,
    MastersRoutingModule,
    NgbModule
  ],
  
  exports: [MastersComponent],
})
export class MastersModule { }
