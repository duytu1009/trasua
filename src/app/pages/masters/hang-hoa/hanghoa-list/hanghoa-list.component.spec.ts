import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HanghoaListComponent } from './hanghoa-list.component';

describe('HanghoaListComponent', () => {
  let component: HanghoaListComponent;
  let fixture: ComponentFixture<HanghoaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HanghoaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HanghoaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
