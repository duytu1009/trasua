import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { DanhMucMonAddComponent } from './danh-muc-mon/danh-muc-mon-add/danh-muc-mon-add.component';
import { HanghoaListComponent } from './hanghoa-list/hanghoa-list.component';
import { DanhMucMonComponent } from './danh-muc-mon/danh-muc-mon.component';


const routes: Routes = [
  {path: '', redirectTo: 'list', pathMatch: 'full'},
  {path: 'list', component: HanghoaListComponent},
];

@NgModule({
  declarations: [HanghoaListComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class HangHoaRoutingModule { }
