import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import { DanhMucMonAddComponent } from './danh-muc-mon-add/danh-muc-mon-add.component';

@NgModule({
  declarations: [DanhMucMonAddComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: 'add',
        component: DanhMucMonAddComponent,
      }
    ])
  ]
})
export class DanhMucMonRoutingModule { }
